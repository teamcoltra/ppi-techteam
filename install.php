<?php
/*---------------------------------------------
Installation File - Should Create Database
----------------------------------------------*/

include('config.php');
echo 'Checking configuration file...<br>';
if ($database == '') {
	die('You did not edit your config.php file. Please update it, then run this script again.')
}
echo 'Success!<br>';
echo 'Creating Users Table...<br>';
$sql = "
CREATE TABLE IF NOT EXISTS `$tableprefix_projects` (
  `projectid` int(255) NOT NULL AUTO_INCREMENT,
  `projectshortdesc` varchar(140) NOT NULL,
  `projectlongdesc` text NOT NULL,
  `projectcreatedby` int(255) NOT NULL,
  `projectreponame` varchar(255) NOT NULL,
  `projectrepohost` varchar(255) NOT NULL,
  `projectrepouser` varchar(255) NOT NULL,
  `projectrepourl` varchar(255) NOT NULL,
  `projecttype` varchar(255) NOT NULL,
  `projectlanguage` varchar(25) NOT NULL,
  `projectname` varchar(255) NOT NULL,
  `projectcreated` datetime NOT NULL,
  `projectupdated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `projectcompleted` int(11) NOT NULL,
  PRIMARY KEY (`projectid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;
";
mysql_query($sql);
echo 'Success!<br>';
echo 'Creating Projects Table...<br>';
$sql = "
CREATE TABLE IF NOT EXISTS `$tableprefix_users` (
  `userid` int(100) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `userfullname` varchar(255) NOT NULL,
  `usertimezone` varchar(5) NOT NULL,
  `userparty` varchar(255) NOT NULL,
  `useremail` varchar(255) NOT NULL,
  `userpassword` varchar(255) NOT NULL,
  `usercreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`userid`),
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;
";
mysql_query($sql);
echo 'Success!<br>';
echo 'Successfully completed installation, please delete the file "install.php" from your server, as it can cause a security risk and more than likely will screw things up if anyone accidently goes to it.<br>';
?>