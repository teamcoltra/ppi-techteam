-- phpMyAdmin SQL Dump
-- version 3.3.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 21, 2012 at 10:42 PM
-- Server version: 5.1.61
-- PHP Version: 5.3.5-1ubuntu7.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ppitech`
--

-- --------------------------------------------------------

--
-- Table structure for table `ppitech_projects`
--

CREATE TABLE IF NOT EXISTS `ppitech_projects` (
  `projectid` int(255) NOT NULL AUTO_INCREMENT,
  `projectshortdesc` varchar(140) NOT NULL,
  `projectlongdesc` text NOT NULL,
  `projectcreatedby` int(255) NOT NULL,
  `projectreponame` varchar(255) NOT NULL,
  `projectrepohost` varchar(255) NOT NULL,
  `projectrepouser` varchar(255) NOT NULL,
  `projectrepourl` varchar(255) NOT NULL,
  `projecttype` varchar(255) NOT NULL,
  `projectlanguage` varchar(25) NOT NULL,
  `projectname` varchar(255) NOT NULL,
  `projectcreated` datetime NOT NULL,
  `projectupdated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `projectcompleted` int(11) NOT NULL,
  PRIMARY KEY (`projectid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ppitech_projects`
--


-- --------------------------------------------------------

--
-- Table structure for table `ppitech_users`
--

CREATE TABLE IF NOT EXISTS `ppitech_users` (
  `userid` int(100) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `userfullname` varchar(255) NOT NULL,
  `usertimezone` varchar(5) NOT NULL,
  `userparty` varchar(255) NOT NULL,
  `useremail` varchar(255) NOT NULL,
  `userpassword` varchar(255) NOT NULL,
  `usercreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`userid`),
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ppitech_users`
--

