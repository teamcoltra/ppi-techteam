<?php
include('config.php');
include('language.php');
//Before Including Template You Can Define Custom Things:
$sitepage = 'Project Page'; //will make the <title> display your current location
//$default_links = '
//            <li class="active"><a href="#">Dashboard</a></li>
//            <li><a href="#">Active</a></li>
//            <li><a href="#">Completed</a></li>
//            <li><a href="#" data-method="delete" rel="nofollow">Logout</a></li>';
//Default Links Changes The Links At Top

$userid = $_SESSION['userid'];
$username = $_SESSION['username'];

include('template.php');
include('atomparser.php');

$projectid = $_GET['id'];

$result = mysql_query("select * from ".$tableprefix."_projects where projectid='$projectid' ")
or die(mysql_error());  
$row=mysql_fetch_array($result);

if($row['projectrepohost'] == 'github') {
  $baserepourl = "https://github.com/{$row['projectrepouser']}/{$row['projectreponame']}";
  $atomurl = "https://github.com/{$row['projectrepouser']}/{$row['projectreponame']}/commits/master.atom";
  $forkurl = $baserepourl;
} else if ($row['projectrepohost'] == 'bitbucket') {
  $baserepourl = "https://bitbucket.org/{$row['projectrepouser']}/{$row['projectreponame']}";
  $atomurl = "https://bitbucket.org/{$row['projectrepouser']}/{$row['projectreponame']}/atom";
  $forkurl = "https://bitbucket.org/{$row['projectrepouser']}/{$row['projectreponame']}/fork";
} else {
  $baserepourl = $row['projectrepourl'];
  $suckstobeyou = 1;
}


echo $head_template;
        $host  = $_SERVER['HTTP_HOST'];
        $uri   = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
        $extra = "user.php?id=$userid";
?>
        <div class="row">
          <div class="span12">
  <div class="box box-nopad" style="overflow:hidden;">

    <div class="span7">
      <!--This is where we will put API-->
      <?php 
      if ($suckstobeyou == 1) {
        echo "<p>Recent Commits Only Works For GitHub and Bitbucket Hosted Repos</p>";
      } else {

$atom_parser = new myAtomParser($atomurl);
$output = $atom_parser->getOutput(2); # returns string containing HTML
echo $output;
}
      ?>
    </div>
    <div class="span4 hero">
      <h1 class="get-organized banner no-border"><?php echo $row['projectname']; ?></h1>
      <div class="signup-link">
        <p class="no-obligation"><small><strong><?php echo $row['projectreponame']; ?></strong></small>
                  <?php if($row['projectcreatedby'] == $userid) { 
              if($row['projectcompleted'] == 0) {
              echo "<a class='btn btn-success' href='submit.php?complete={$row['projectid']}'>Mark Completed</a>";
            } else if($row['projectcompleted'] == 1) {
              echo "<a class='btn btn-danger' href='submit.php?reopen={$row['projectid']}'>Reopen Project</a>";
            } else {
              echo "<a class='btn btn-success' href='submit.php?complete={$row['projectid']}'>Mark Completed</a>";
            }
          }
          ?></p>
      </div>
    </div>

    <div class="row row45">
      <div class="span12">
        <hr />

        <h1 class="banner no-border"><?php echo $row['projectshortdesc']; ?></h1>

        <div class="row">
          <div class="span8 offset2">
            <p><?php echo $row['projectlongdesc']; ?></p>
          </div>
        </div>

      </div>
    </div>

    <div class="row row45">
      <div class="span12">
        <hr />

        <h1 class="banner no-border">Are You In?</h1>

        <div style="text-align:center;">
        <p><a href="mailto:useraddress" class="btn btn-signup btn-info btn-large">Email Project Manager</a> <?php if ($suckstobeyou == 1) { /*no*/ } else { echo "<a href='$forkurl' class='btn btn-signup btn-success btn-large'>Fork It</a>"; } ?></p>
        <p>Fork/Clone: <code>git clone <?php if ($suckstobeyou == 1) { echo $row['projectrepourl']; } else { echo $baserepourl.".git"; } ?></code></p>
        <p><small><strong>Also, remember you can always discuss on the mailing list.</strong></small></p>
      </div>
      </div>


  </div>
</div>

        </div><!--/row-->
 <?php
echo $foot_template;
?>

